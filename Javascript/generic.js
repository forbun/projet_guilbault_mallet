function num_translate(num)
{
	if(num < 10)
	{
		return "0"+num;
	}
	else
	{
		return num;
	}
}

function get_jour(date)
{
	switch(date.getDay())
		{
			case 1:
				return "LUNDI";
				break;
			case 2:
				return "MARDI";
				break;
			case 3:
				return "MERCREDI";
				break;
			case 4:
				return "JEUDI";
				break;
			case 5:
				return "VENDREDI";
				break;
			case 6:
				return "SAMEDI";
				break;
			case 0:
				return "DIMANCHE";
				break;
			default:
				return 1;
				break;
		}
}

function get_mois(date)
{
	var tab_mois = ["JANVIER", "FEVRIER", "MARS", "AVRIL", "MAI", "JUIN", "JUILLET", "AOUT", "SEPTEMBRE", "OCTOBRE", "NOVEMBRE", "DECEMBRE"];
	return tab_mois[date.getMonth()];
}

function set_date()
{
	var d = new Date();
	var string = get_jour(d)+' '+num_translate(d.getDate())+' '+get_mois(d)+' '+d.getFullYear();
	document.getElementById("date_area").value = string;
}

function submit_form()
{
	var formulaire = window.document.getElementById("formulaire");
	formulaire.submit();
}
function collect_elts()
{
	//Get only input text in the form
	var frm = window.document.getElementsByTagName('form')[0];
	var elts =frm.getElementsByTagName("input");
	
	var j = 0;
	eltsS=[0,0];
	for(var i = 0; i < elts.length;i++)
	{
		if(elts[i].type == "text" && elts[i].value != "")
		{
			eltsS[j] = elts[i];
			j ++;
		}
	}
	return eltsS;
}

function letter_to_number(a)
{
	var table = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];
	var table2 = ["A","B","C","D","E","R","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
	for(var i = 0; i < table.length;i++)
	{
		if(a == table[i] || a == table2[i])
		{
			return i;
		}
	}
	return a;
}

function encrypt_value()
{
	var elts = collect_elts();
	var random_secret = 12; /*not a place holder, I actually threw a dice*/
	for(var i = 0; i < elts.length; i++)
	{
		var val = elts[i].value;
		crypted_val="";
		for (var j = 0;j < val.length; j++)
		{
			crypted_val+=num_translate(letter_to_number(val[j]));
		}
		/*now that the value field has been changed to numbers lets scramble it with our secret ingredient*/
		crypted_val = parseInt(crypted_val);
		crypted_val -= random_secret;
		/*and re inject it in the page*/
		elts[i].value = crypted_val;
	}
}

//Generate nav Arrow div (prev/next navigation)
function genFootNav(currPage) {
	var nf = document.getElementById('navArrow');
	
	//What is the next and the previous pages ?
	var prevPage = '';
	var nextPage = '';
	switch(currPage) {
		case 'index':
			nextPage = 'page1.html';
			break;
		case 'page1':
			prevPage = 'index.html';
			nextPage = 'page2.html';
			break;
		case 'page2':
			prevPage = 'page1.html';
			nextPage = 'page3.html';
			break;
 		case 'page3':
			prevPage = 'page2.html';
			nextPage = 'page4.html';
			break;
 		case 'page4':
			prevPage = 'page3.html';
			break;
		default:
			break;
	}

	//Create 3 elements <img> each nested in <a> element
	var img;
	var lnk;
	//Precedent
	img = document.createElement('img');
		img.src = '../Images/generic/left.png';
		img.alt = 'Précédent';
		img.title='page précédente';
	if(prevPage != '') {
		lnk = document.createElement('a'); 
		lnk.href = prevPage;
		nf.appendChild(lnk);
		lnk.appendChild(img);
	} else {
		nf.appendChild(img);
	}
	//Sommaire
	img = document.createElement('img');
	lnk = document.createElement('a');
		img.src = '../Images/generic/index.png';
		img.alt = 'Sommaire';
		img.title='sommaire';
 		lnk.href = 'index.html';
		nf.appendChild(lnk);
		lnk.appendChild(img); 
	//Suivant
 	img = document.createElement('img');
		img.src = '../Images/generic/right.png';
		img.alt = 'Suivant';
		img.title="page suivante";
	if(nextPage != '') {
	 	lnk = document.createElement('a'); 
 		lnk.href = nextPage;
		nf.appendChild(lnk);
		lnk.appendChild(img); 
	} else {
		nf.appendChild(img);
	}
}

//Generate nav div with Links (header)
function genHeadNav(currPage) {
	var nh = document.getElementById('navLinks');	
	//Coming soon : what are next/prev pages ?
	
	//Create var for ul, li and a elements
	var unli1; //<ul> niv1 (list of pages)
	var unli2; //<ul> niv2 (list of anchors in pages)
	var liit1; //<li> niv1 (pages)
	var liit2; //<li> niv2 (anchors)
	var anch; //<a>
	var txtN; //#text
	
	//Add a <li> element in $domElem, with a link to $href, which content $text
	//Return the <li> element
	function addLiElem(domElem, href, txt) {
		var li = document.createElement('li');
			domElem.appendChild(li);
		var a = document.createElement('a');
			a.href = href;
			li.appendChild(a);
		var txtN = document.createTextNode(txt);
			a.appendChild(txtN);
		
		return li;
	}
	
	//Add a <ul> element in $domElem
	//Return the <ul> element

	function addUlElem(domElem) {
		var ul = document.createElement('ul');
			domElem.appendChild(ul);
		return ul;
	}
	
	//Build the navigator with links
	
	unli1 = addUlElem(nh);
	//Page1
	liit1 = addLiElem(unli1, "page1.html#welcome", "Page1");
	unli2 = addUlElem(liit1);
	addLiElem(unli2, "page1.html#presentation_title", "Présentation");
	addLiElem(unli2, "page1.html#syabh", "Sir You Are Being Hunted");

	//Page2
	liit1 = addLiElem(unli1, "page2.html#LoLh1", "Page2");
	unli2 = addUlElem(liit1)  
	addLiElem(unli2, "page2.html#presentation", "Présentation de League of Legends");
	addLiElem(unli2, "page2.html#statJoueurs", "Statistiques sur les joueurs");
	addLiElem(unli2, "page2.html#champions", "Les Champions");


	//Page3
	liit1 = addLiElem(unli1, "page3.html#formTitle1", "Page3");
	unli2 = addUlElem(liit1);
	addLiElem(unli2, "page3.html#questionnaire", "Début du questionnaire");
	addLiElem(unli2, "page3.html#jeuVideo", "Les jeux vidéos et vous");
	addLiElem(unli2, "page3.html#hardware", "Sur quelles plate formes jouez-vous ?");
	addLiElem(unli2, "page3.html#souvenir", "Faites part de vos expériences");



	//Page4
	liit1 = addLiElem(unli1, "page4.html#p4_title1", "Page4");
	unli2 = addUlElem(liit1);
	addLiElem(unli2, "page4.html#p4studios", "Studios indépendants");
	addLiElem(unli2, "page4.html#sources", "Sources");

}


//fonction mettant les liens au premier plan une fois en fin de page
function popLinks()
{
	if(document.documentElement.scrollTop == document.documentElement.scrollTopMax)
	{
		var nav = document.getElementById('navLinks');	
		nav.setAttribute("style","z-index:1;");
	}
}

function greenIfChecked()
{
	var list = document.getElementsByTagName("input");
	for(var i = 0;i < list.length;i++)
	{
		if(list[i].type == "checkbox")
		{
			if(list[i].checked)
			{
				var tmp = list[i].nextSibling;
				tmp = tmp.nextSibling;
				tmp.setAttribute("style","color:green;");
			}
			else
			{
				var tmp = list[i].nextSibling;
				tmp = tmp.nextSibling;
				tmp.setAttribute("style","color:white;");
			}
		}
	}
}

//Fonction appelée au chargment de la page
function buildPage(currPage) {
	alert("Pour accéder au menu et le mettre au premier plan veuillez scroller jusqu'à la fin de la page");
	set_date();
	if(currPage != 'index') {
		genHeadNav(currPage);
	}
	genFootNav(currPage);
	setInterval("popLinks()",1);
	setInterval("greenIfChecked()",1);
}



// Question c. : affiche les valeurs des données du formulaire dans un tableau d'id #formVal
function showValForm() {
	// @function : ajoute une ligne dans $domElem avec 2 cellules de type $cellType (th ou td), contenant chacun $inptype, $prop et $val
	function addRow(domElem, type, inpType, prop, val) {
		var txtNod; //Noeud de texte
		var tr = document.createElement('tr');
			tbody.appendChild(tr);
		var tdInpType = document.createElement(type);
			tr.appendChild(tdInpType);
			txtNod = document.createTextNode(inpType);
			tdInpType.appendChild(txtNod);
		var tdProp = document.createElement(type);
			tr.appendChild(tdProp);
			txtNod = document.createTextNode(prop);
			tdProp.appendChild(txtNod);
		var tdVal = document.createElement(type);
			tr.appendChild(tdVal);
			txtNod = document.createTextNode(val);
			tdVal.appendChild(txtNod);
	}

	//Get all informations from <form> of opener
	var frm = opener.document.getElementsByTagName('form')[0];
	var inputs = frm.getElementsByTagName('input');
	var txtarea = frm.getElementsByTagName('textarea');
	var slct = frm.getElementsByTagName('select');

	//Open a new window, and get the <table> to show all values
	var tbl = document.getElementById('formVal');
	var tbody = document.createElement('tbody');
	tbl.appendChild(tbody);
	
	// @info : Le tableau sera de type Propriété / Valeur
	//Crée la premiere ligne (en-tete)
	addRow(tbody, 'th', 'type', 'Nom', 'Valeur');
 
	//Add all values
	//Inputs
	for(var i = 0 ; i < inputs.length ; i++) {
		switch(inputs[i].type) {
			case 'radio':
				if(inputs[i].checked) {
					addRow(tbody, 'td', 'input.radio', inputs[i].name, inputs[i].value);
				}
				break;
			case 'checkbox':
				if(inputs[i].checked) {
					addRow(tbody, 'td', 'input.checkbox', inputs[i].name, 'Checked: On');
				}
				break;
			case 'text':
				addRow(tbody, 'td', 'input.text', inputs[i].name, '"' + inputs[i].value + '"');
				break;
			default:
				break;
		}
	}
	//Textarea (one instance)
	for(var i = 0 ; i < txtarea.length ; i++) {
		addRow(tbody, 'td', 'textarea', txtarea[i].name, '"' + txtarea[i].value + '"');
	}
	//Select
	for(var i = 0 ; i < slct.length ; i++) {
		addRow(tbody, 'td', 'select', slct[i].name, slct[i].options[slct[i].selectedIndex].childNodes[0].data);
		//childNodes[0] : Selectionne le noeud texte de <option>
	}
	
}

